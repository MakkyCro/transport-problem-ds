import numpy as np
import random
import operator
import sys
import unittest
from heapq import nsmallest


class MatrixError(Exception):
    pass


class Matrix(object):

    def __init__(self, m, n, init=True):
        if init:
            self.rows = [[0]*n for x in range(m)]
        else:
            self.rows = []
        self.m = m
        self.n = n

    def __getitem__(self, idx):
        return self.rows[idx]

    def __setitem__(self, idx, item):
        self.rows[idx] = item

    def __str__(self):
        s = '\n'.join([' '.join([str(item) for item in row])
                       for row in self.rows])
        return s + '\n'

    def getRank(self):
        return (self.m, self.n)

    @classmethod
    def _makeMatrix(cls, rows):

        m = len(rows)
        n = len(rows[0])
        # if any([len(row) != n for row in rows[1:]]):
        # raise MatrixError, "inconsistent row length"
        mat = Matrix(m, n, init=False)
        mat.rows = rows

        return mat

    @classmethod
    def makeRandom(cls, m, n, low=0, high=10):
        obj = Matrix(m, n, init=False)
        for x in range(m):
            obj.rows.append([random.randrange(low, high)
                             for i in range(obj.n)])

        return obj

    @classmethod
    def readEntry(cls):
        print("Unesite vrijednosti redak po redak, tako da vrijednosti odvojite razmakom. Upišite \"q\" za potvrdu")
        rows = []
        while True:
            line = sys.stdin.readline().strip()
            if line == 'q':
                break

            row = [int(x) for x in line.split()]
            rows.append(row)

        return cls._makeMatrix(rows)

    @classmethod
    def readGrid(cls, fname):
        """ Možda će nam trebat kod scrapanja- citanje iz datoteke"""

        rows = []
        for line in open(fname).readlines():
            row = [int(x) for x in line.split()]
            rows.append(row)

        return cls._makeMatrix(rows)

    @classmethod
    def fromList(cls, listoflists):
        """ Kod scrapanja da iz liste generiramo matricu """
        # Primjer: matrica =  Matrix.fromList([[1 2 3], [4,5,6], [7,8,9]])

        rows = listoflists[:]
        return cls._makeMatrix(rows)

    def NWCmetoda(self, ponuda, potraznja):
        tmp = np.zeros((self.m, self.n))
        i = 0
        j = 0
        trosak = 0
        tmp_ponuda = ponuda
        tmp_potraznja = potraznja

        while(True):
            if tmp_potraznja[j] == 0:
                j += 1
            if tmp_ponuda[i] == 0:
                i += 1
            if i == self.m and j == self.n:
                break
            if tmp_potraznja[j] > 0:
                if tmp_potraznja[j] >= tmp_ponuda[i]:
                    tmp[i][j] = ponuda[i]
                    tmp_potraznja[j] -= tmp_ponuda[i]
                    trosak += tmp[i][j]*self[i][j]
                    tmp_ponuda[i] = 0
                elif tmp_potraznja[j] < tmp_ponuda[i]:
                    tmp[i][j] = tmp_potraznja[j]
                    tmp_ponuda[i] -= tmp_potraznja[j]
                    trosak += tmp[i][j]*self[i][j]
                    tmp_potraznja[j] = 0
            # print(tmp, "\nPonuda:", ponuda, "\nPotražnja:",potraznja)
        print("\n\nNWC metoda:\n", tmp, "\nT =", trosak)

    def LCmetoda(self, ponuda, potraznja):
        tmp_matrica = self
        tmp_ponuda = ponuda
        tmp_potraznja = potraznja
        tmp = np.zeros((self.m, self.n))
        i = 0
        j = 0
        uk_trosak = 0

        koordinate = [0, 0]

        while(True):
            max_t = 0
            minimum = 100000
            for i in range(0, len(ponuda)):
                for j in range(0, len(potraznja)):
                    tmp_max_t = min(tmp_ponuda[i], tmp_potraznja[j])

                    if tmp_matrica[i][j] < minimum and tmp_ponuda[i] != 0 and tmp_potraznja[j] != 0:
                        max_t = min(tmp_ponuda[i], tmp_potraznja[j])
                        minimum = tmp_matrica[i][j]
                        koordinate = [i, j]

                    elif tmp_matrica[i][j] == minimum and tmp_ponuda[i] != 0 and tmp_potraznja[j] != 0:
                        if tmp_max_t > max_t:
                            max_t = min(tmp_ponuda[i], tmp_potraznja[j])
                            minimum = tmp_matrica[i][j]
                            koordinate = [i, j]

                    # print(minimum)

            tmp[koordinate[0], koordinate[1]] = max_t
            # print(tmp, "\nMinimum:", minimum, "\nMax spremanje:", max_t, "\nKoordinate:", koordinate,"\nPonuda:", tmp_ponuda, "\nPotražnja:",tmp_potraznja)
            uk_trosak += max_t*tmp_matrica[koordinate[0]][koordinate[1]]
            if max_t == tmp_ponuda[koordinate[0]]:
                tmp_potraznja[koordinate[1]] -= tmp_ponuda[koordinate[0]]
                tmp_ponuda[koordinate[0]] = 0
            elif max_t == tmp_potraznja[koordinate[1]]:
                tmp_ponuda[koordinate[0]] -= tmp_potraznja[koordinate[1]]
                tmp_potraznja[koordinate[1]] = 0

            if all(v == 0 for v in tmp_potraznja) and all(v == 0 for v in tmp_ponuda):
                break

        print("\n\nLeast Cost metoda:\n", tmp, "\nT =", uk_trosak)

    @classmethod
    def column(cls, matrix, i):
        return [row[i] for row in matrix]

    def Vogelovametoda(self, ponuda, potraznja):
        tmp_matrica = self
        tmp_ponuda = ponuda
        tmp_potraznja = potraznja
        tmp = np.zeros((self.m, self.n))
        n_raz = np.zeros(self.m)
        m_raz = np.zeros(self.n)
        i = 0
        j = 0
        uk_trosak = 0

        koordinate = [0, 0]

        while(True):
            max_t = 0
            for i in range(0, len(ponuda)):
                najmanji_br = nsmallest(2, tmp_matrica[i])
                if najmanji_br[1] == 1000000:
                    najmanji_br[1] = najmanji_br[0]
                    najmanji_br[0] = 0.0
                n_raz[i] = najmanji_br[1] - najmanji_br[0]
                if n_raz[i] >= 100000:
                    n_raz[i] = 0

            for i in range(0, len(potraznja)):
                kolona = self.column(tmp_matrica, i)
                najmanji_br = nsmallest(2, kolona)
                if najmanji_br[1] == 1000000:
                    najmanji_br[1] = najmanji_br[0]
                    najmanji_br[0] = 0.0
                m_raz[i] = najmanji_br[1] - najmanji_br[0]
                if m_raz[i] >= 100000:
                    m_raz[i] = 0

            if max(n_raz) > max(m_raz):
                for i in range(0, len(ponuda)):
                    if n_raz[i] == max(n_raz):
                        minimum_matrice = min(tmp_matrica[i])
                        for j in range(0, len(potraznja)):
                            if max_t < min(ponuda[i], potraznja[j]) and minimum_matrice == tmp_matrica[i][j] and tmp_ponuda[i] != 0 and tmp_potraznja[j] != 0:
                                max_t = min(ponuda[i], potraznja[j])
                                koordinate = [i, j]
            elif max(m_raz) > max(n_raz):
                for j in range(0, len(potraznja)):
                    if m_raz[j] == max(m_raz):
                        minimum_matrice = min(self.column(tmp_matrica, j))
                        for i in range(0, len(ponuda)):
                            if max_t < min(ponuda[i], potraznja[j]) and minimum_matrice == tmp_matrica[i][j] and tmp_ponuda[i] != 0 and tmp_potraznja[j] != 0:
                                max_t = min(ponuda[i], potraznja[j])
                                koordinate = [i, j]

            else:
                for i in range(0, len(ponuda)):
                    if n_raz[i] == max(n_raz):
                        minimum_matrice = min(tmp_matrica[i])
                        for j in range(0, len(potraznja)):
                            if max_t < min(ponuda[i], potraznja[j]) and minimum_matrice == tmp_matrica[i][j] and tmp_ponuda[i] != 0 and tmp_potraznja[j] != 0:
                                max_t = min(ponuda[i], potraznja[j])
                                koordinate = [i, j]
                for j in range(0, len(potraznja)):
                    if m_raz[j] == max(m_raz):
                        minimum_matrice = min(self.column(tmp_matrica, j))
                        for i in range(0, len(ponuda)):
                            if max_t < min(ponuda[i], potraznja[j]) and minimum_matrice == tmp_matrica[i][j] and tmp_ponuda[i] != 0 and tmp_potraznja[j] != 0:
                                max_t = min(ponuda[i], potraznja[j])
                                koordinate = [i, j]

            tmp[koordinate[0], koordinate[1]] = max_t
            uk_trosak += max_t*tmp_matrica[koordinate[0]][koordinate[1]]
            if max_t == tmp_ponuda[koordinate[0]]:
                tmp_potraznja[koordinate[1]] -= tmp_ponuda[koordinate[0]]
                tmp_ponuda[koordinate[0]] = 0
                n_raz[koordinate[0]] = -1
                for l in range(0, len(potraznja)):
                    tmp_matrica[koordinate[0]][l] = 1000000
                    #tmp_matrica[koordinate[0]] = 1000000
            elif max_t == tmp_potraznja[koordinate[1]]:
                tmp_ponuda[koordinate[0]] -= tmp_potraznja[koordinate[1]]
                tmp_potraznja[koordinate[1]] = 0
                m_raz[koordinate[1]] = -1
                for k in range(0, len(ponuda)):
                    tmp_matrica[k][koordinate[1]] = 1000000

            if all(v == 0 for v in tmp_potraznja) and all(v == 0 for v in tmp_ponuda):
                break

        print("\n\nVogelova metoda:\n", tmp, "\nT =", uk_trosak)


if __name__ == "__main__":
    unittest.main()
