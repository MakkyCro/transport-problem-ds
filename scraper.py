from bs4 import BeautifulSoup
import requests
import numpy as np

url = 'http://test.victory.hr/DS/'
page = requests.get(url)

soup = BeautifulSoup(page.content, 'html.parser')
tr = soup.find_all("tr")
th = soup.find_all("th")
results = soup.find_all("td")

n = len(tr)
m = len(th)

tmp = []
s_pon = []
s_pot = []

pon = soup.find_all("td",{"class":"pon"})
for p in pon:
    s_pon.append(int(p.text))

pot = soup.find_all("td",{"class":"pot"})
for p in pot:
    s_pot.append(int(p.text))

trosak = soup.find_all("td",{"class":"trosak"})
for t in trosak:
    tmp.append(int(t.text))


s_matrica_tr = np.resize(tmp, len(tmp)).reshape(3,4)


