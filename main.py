from matrix import Matrix
from scraper import s_matrica_tr, s_pon, s_pot, url, n as N, m as M
from mpi4py import MPI
import sys

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

odgovor = None

if rank == 0:

    while (True):
        odgovor = input("Upišite broj 1 ukoliko želite sami popuniti tablicu troškova, broj 2 ukoliko želite da se tablica napuni nasumičnim vrijednostima ili broj 3 ukoliko želite preuzeti testne podatke sa interneta. Pritisnite CTRL + C za izlaz.\n\n")
        
        if odgovor == "1":
            matrica_troskova = Matrix.readEntry()
            velicina = matrica_troskova.getRank()
            n = velicina[0]
            m = velicina[1]
            ponuda = []
            potraznja = []
            break
        elif odgovor == "2":
            unos = list(map(int, input(
                "Unesite redom broj redaka, stupaca matrice te minimalnu i maksimalnu vrijednost odvojeno razmakom: (primjer - 2 3 1 100)").split(" ", 4)))
            matrica_troskova = Matrix.makeRandom(unos[0], unos[1], unos[2], unos[3])
            n = unos[0]
            m = unos[1]
            ponuda = []
            potraznja = []
            break
        elif odgovor == "3":
            print("Testni podaci preuzeti sa:", url)
            matrica_troskova = Matrix.fromList(s_matrica_tr)
            n = N
            m = M
            ponuda = s_pon
            potraznja = s_pot
            break
        else:
            odgovor = None
            matrica_troskova = None
            n = 0
            m = 0
            ponuda = []
            potraznja = []
            print("Netočan unos!")
        

    # Provjera zatvorenosti
    while(True):
        suma_pon = 0
        suma_pot = 0

        if odgovor != "3":
            ponuda.clear()
            potraznja.clear()
            print("NAPOMENA: zbroj vrijednosti ponude i potražnje mora biti jednak.")
            unos = list(map(int, input(
                "Unesite ponudu (n brojeva) i potražnju (m brojeva) razmaknutih razmakom: ").split(" ", n+m)))

            for rb, vrijednost in enumerate(unos, start=0):
                if rb < n:
                    ponuda.append(vrijednost)
                    suma_pon += vrijednost
                else:
                    potraznja.append(vrijednost)
                    suma_pot += vrijednost
        else:
            suma_pon = sum(ponuda)
            suma_pot = sum(potraznja)

        if suma_pon == suma_pot:
            break
        else:
            print("Netočan unos ponude i potražnje.")

        
if rank == 0:
    print("\nMatrica koju rješavamo:\n")
    print(str(matrica_troskova),"\nPonuda:", ponuda, "\nPotraznja:", potraznja)
    sendmsg = (matrica_troskova,ponuda,potraznja)
else:
    sendmsg = None

comm.Barrier()

recvmsg = comm.bcast(sendmsg, root=0)

matrica_troskova = recvmsg[0]
ponuda = recvmsg[1]
potraznja = recvmsg[2]

if rank == 0:
    tmp_pon = ponuda.copy()
    tmp_pot = potraznja.copy()
    Matrix.NWCmetoda(matrica_troskova, tmp_pon, tmp_pot)

comm.Barrier()

if rank == 1:
    tmp_pon = ponuda.copy()
    tmp_pot = potraznja.copy()
    Matrix.LCmetoda(matrica_troskova, tmp_pon, tmp_pot)

comm.Barrier()

if rank == 2:
    tmp_pon = ponuda.copy()
    tmp_pot = potraznja.copy()
    Matrix.Vogelovametoda(matrica_troskova, ponuda, potraznja)